// Array.forEach
let list = [4, 5, 6];
list.forEach((value, index) => {
  console.log(`index ${index} values ${value}`);
}); //0,1,2 as well 4,5,6

// Array.filter
let list = [2, 1, 4, 5, 7, 9, 8];
let result = list.filter((e) => e > 4);
console.log(result); //[5,7,9,8]

// Array.map
let list = [
  { name: "akshay", age: 25 },
  { name: "rahul", age: 26 },
  { name: "anand", age: 24 },
];
let result = list.map((e)=>{
    return e.name
})
console.log(result); //[ 'akshay', 'rahul', 'anand' ]

// Array.reduce
let list = [2, 6, 1, 5, 4, 9, 8];
let result = list.reduce((acc,e) => {
    acc += e;
    return acc;
})
console.log(result); //35

// Array.sort
let list = [2, 5, 1, 6, 9, 8, 4, 3];
console.log(list.sort((a,b)=>a-b)); // [1, 2, 3, 4, 5, 6, 8, 9]


