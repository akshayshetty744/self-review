// for in loop
let list = [4, 5, 6];
for (let i in list) {
    console.log(i);
} // 0,1,2

//for of loop
let list = [4, 5, 6];
for (let i of list) {
  console.log(i);
} // 4,5,6

// forEach loop
let list = [4, 5, 6];
list.forEach((value, index) => {
  console.log(`index ${index} values ${value}`);
}); //0,1,2 as well 4,5,6

//for loop
let list = [4, 5, 6];
for (let i = 0; i < list.length; i++) {
  console.log(list[i]);
}// 4, 5, 6

//while loop
let i = 0;
while (i < 5) {
    console.log(i);
    i++
}//0,1,2,3,4
