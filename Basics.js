//Array.pop
// Mutable
let array = [4, 5, 6, 7, 8];
 array.pop();
console.log(array);//[4,5,6,7]
 
//Array.push
Mutable
let array = [4, 5, 6, 7, 8];
array.push(9);
console.log(array);//[4, 5, 6, 7,8,9]

//Array.concat
//immutable
let array1 = [4, 5, 6, 7, 8];
let array2 = [1, 2, 3];
console.log(array2.concat(array1));//[1,2,3,4, 5, 6, 7,8,9]

//Array.slice
//immutable
let array = [1, 2, 3, 4, 5, 6, 7];
console.log(array.slice(2, 5));//[3, 4,5]

//Array.splice
// immutable
let array = [1, 2, 3, 4, 5, 6, 7];
array.splice(1, 2,'akshay');
console.log(array)//[ 1, 'akshay', 4, 5, 6, 7 ]

//Array.join
let array = ["akshay","rohan","anand"];
console.log(array.join("-")); //"akshay-rohan-anand"

//Array.flat
let array = [1, 2, 3, [4, 5, 6, [7, 8, 9]]];
console.log(array.flat(2));//[1, 2, 3, 4, 5, 6, 7, 8, 9]

