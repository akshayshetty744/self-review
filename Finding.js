// Array.find
let array = [1, 2, 3, 4, 5, 6];
let result = array.find((ele) => ele>4);
console.log(result);//5

// Array.indexOf
let list = [1, 2, 3, 4, 5, 6];
console.log(list.indexOf(4));//3

// Array.includes
let list = [1, 2, 3, 4, 5, 6];
console.log(list.includes(4));// true

// Array.findIndex
let list = [1, 2, 3, 4, 5];
let result = list.findIndex((e) => e > 3);
console.log(result);//3

